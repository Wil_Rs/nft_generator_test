import React, { useState } from "react";
import axios from 'axios'
import Formdata from 'form-data'
import 'bootstrap/dist/css/bootstrap.min.css';

import { ethers } from 'ethers'

import loading from './assets/loading.svg'

let tokenPinata = ''

const App2 = () => {
    const [msg, setMsg] = useState({IpfsHash: '...'})
    const [enviando, setEnviando] = useState(false)

    const [balance, setBalance] = useState();

    const [tokenImage, setTokenImage] = useState()
    const [account, setAccount] = useState()
    
    const getBalance = async () => {
        const [account] = await window.ethereum.request({ method: 'eth_requestAccounts' });
        setAccount( account )
        const provider = new ethers.providers.Web3Provider(window.ethereum);
        const balance = await provider.getBalance(account);
        setBalance(ethers.utils.formatEther(balance));
    };

    let FiredGuys = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"approved","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"operator","type":"address"},{"indexed":false,"internalType":"bool","name":"approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"approve","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"getApproved","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"uri","type":"string"}],"name":"isContentOwned","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"ownerOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"string","name":"metadataURI","type":"string"}],"name":"payToMint","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"},{"internalType":"string","name":"uri","type":"string"}],"name":"safeMint","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"operator","type":"address"},{"internalType":"bool","name":"approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"tokenURI","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"from","type":"address"},{"internalType":"address","name":"to","type":"address"},{"internalType":"uint256","name":"tokenId","type":"uint256"}],"name":"transferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]
    const contractAddress = '';

    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = provider.getSigner();

    console.log('signer: ', signer )

    const contract = new ethers.Contract(contractAddress, FiredGuys, signer);

    const uploadPinataJson = (hashImg) => {
        let url = 'https://api.pinata.cloud/pinning/pinJSONToIPFS'

        let nameImg = document.querySelector('#file').files[0].name
        nameImg = nameImg.slice(0, nameImg.lastIndexOf('.'))

        console.log(' hashImg : ', hashImg)

        setEnviando(true)

        let jsonBody = {
            pinataMetadata: {
                name: `${nameImg}.json`
            },
            pinataContent: {
                name:`${nameImg}`,
                symbol:"",
                image:`https://gateway.pinata.cloud/ipfs/${hashImg}`
            }
        }

        axios.post(url, jsonBody, {
            headers: {'Authorization': `Bearer ${tokenPinata}`}
        }).then(e => {
            e.data['json'] = 'mintar'
            setMsg(e.data)
            setEnviando(false)

        }).catch(e => {
            setMsg(e.data)
        })
    }

    const uploadPinataFile = () => {
        let url = 'https://api.pinata.cloud/pinning/pinFileToIPFS'
        let form = new Formdata();
        let file = document.querySelector('#file').files[0]

        setEnviando(true)
        
        form.append('file', file)
        
        axios.post(url, form, {
            headers: {'Authorization': `Bearer ${tokenPinata}`}
        }).then(e => {
            setMsg(e.data)
            setEnviando(false)
            setTokenImage(e.data.IpfsHash)
            uploadPinataJson(e.data.IpfsHash)
        }).catch(e => {
            setMsg(e)
            setEnviando(false)
        })
    }

    const mintToken = async () => {
        if( window.ethereum.isConnected() ){
            // let metadataURI = 'https://gateway.pinata.cloud/ipfs/QmQYP7MqSqsvny1VEwbJJysLJWZ9xBFGRxN5S89dKRn5g2'
            let metadataURI = `https://gateway.pinata.cloud/ipfs/${tokenImage}`
            const connection = contract.connect(signer);
            const addr = connection.address;
            const result = await contract.payToMint(addr, metadataURI, {
            value: ethers.utils.parseEther('0.005'),
            });
    
            await result.wait();
            console.log(result)
        } else {
            alert('Conectar na carteira')
            getBalance()
        }
      };

      const [link, setLink] = useState()
      const imgLink = () => {
        let img = document.querySelector('#file').files[0]
        setLink( URL.createObjectURL(img) )
      }

    return(
        <div className="container-fluid">
            <div className="row">
                
                <div className="col-6">
                    <img src={link} alt="" width={300} />
                </div>
                <div className="col-6 d-flex">
                    <input className="btn btn-danger w-100 p-5" type="file" name="file" id="file" onChange={imgLink}/>
                </div>
                <div>
                    <button className="btn btn-success w-100 p-5" onClick={uploadPinataFile}>enviar</button>
                </div>
            </div>

            <div className="row pt-5">
                <div className="col-6">
                    <button className="btn btn-warning" onClick={getBalance}>wallet</button>
                    <span className="border p-3">Saldo carteira: {balance}</span>
                </div>
            </div>

            <div className="row pt-5 mt-5 border">
                {tokenImage ?
                    <button onClick={mintToken}>mint</button>
                    :
                    <p>nao tem</p>
                }
            </div>

            <div className="row border mt-5">
                <div className="result">
                    {enviando ? 
                        <img src={loading} alt="loading"/>
                        :
                        <>
                            <p>{msg.IpfsHash}</p>
                            <p>{msg.PinSize}</p>
                            <p>{msg.Timestamp}</p>
                            <p>{msg.json}</p>
                        </>
                        }
                </div>
            </div>
        </div>

    )
}

export default App2