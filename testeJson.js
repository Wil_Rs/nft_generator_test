const axios = require('axios')
let tokenPinata = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySW5mb3JtYXRpb24iOnsiaWQiOiI1Mjg3YTEwNy03ZGVjLTQzNDEtYjA1Ni0zYjQ5Y2FjZDBkMzgiLCJlbWFpbCI6IndpbGlhbi5yb2RyaWd1ZXNAc2F1ZGVnYXJhbnRpZGEuY29tLmJyIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsInBpbl9wb2xpY3kiOnsicmVnaW9ucyI6W3siaWQiOiJOWUMxIiwiZGVzaXJlZFJlcGxpY2F0aW9uQ291bnQiOjF9XSwidmVyc2lvbiI6MX0sIm1mYV9lbmFibGVkIjpmYWxzZX0sImF1dGhlbnRpY2F0aW9uVHlwZSI6InNjb3BlZEtleSIsInNjb3BlZEtleUtleSI6Ijc4ZWU3NjkyNzg3Njg0YTE3ODlmIiwic2NvcGVkS2V5U2VjcmV0IjoiMmRmNjE0OWJkYTliNDBmOTMxZDg5MTM4ZDZjZTA4MTEzOGIwN2Q5OGI1YWQwZDJlYzIwMzYxZGYzODljNDY3ZiIsImlhdCI6MTY0NjQwMTE0NH0.WWHUYeHEzBfQed8FHstnt-2g-Gj1uEVXk-_AeBpQcSo'

const uploadPinataJson = (hashImg) => {
    let url = 'https://api.pinata.cloud/pinning/pinJSONToIPFS'

    let nameImg = 'umaImagem'
    // nameImg = nameImg.slice(0, nameImg.lastIndexOf('.'))

    console.log(' hashImg : ', hashImg)

    // setEnviando(true)

    let json = {
        pinataMetadata: {
            name: `${nameImg}.json`
        },
        pinataContent: {
            name:`${nameImg}`,
            symbol:"",
            image:`https://gateway.pinata.cloud/ipfs/${hashImg}`
        }
    }

    console.log(json)
    // return

    axios.post(url, json, {
        headers: {'Authorization': `Bearer ${tokenPinata}`}
    }).then(e => {
        // setMsg(e.data)
        // setEnviando(false)
        console.log('Deu certo')
        console.log(e.data)

    }).catch(e => {
        // setMsg(e.data)
        console.log('Deu errado :(')
        console.log(e.response.data)
    })
}

console.log('iniciando...')

uploadPinataJson('minhahash')